# Botket

## Install

Install the dependancies:

`cargo install`

## Run

`cargo run` will run the app but if you want to keep it running and reloading when ever a file changes you can use [watchexec](https://github.com/mattgreen/watchexec). And run with:

`watchexec -r cargo run`

