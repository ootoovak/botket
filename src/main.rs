#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate slack;

use std::thread;
use rocket::config;

struct Botket;

#[allow(unused_variables)]
impl slack::EventHandler for Botket {
    fn on_event(&mut self,
                cli: &mut slack::RtmClient,
                event: Result<slack::Event, slack::Error>,
                raw_json: &str) {
        println!("Event:");
        println!("\t{:?}", event);
        let ignored = "Ignored event for now.".to_string();
        match event {
            Ok(slack::Event::Message(slack::Message::Standard { text, .. })) => {
                let _ = cli.send_message("#general", &text.unwrap());
            }
            _ => println!("\t{:?}", event),
        }
    }

    fn on_ping(&mut self, cli: &mut slack::RtmClient) {
        println!("--- pinged");
    }

    fn on_close(&mut self, cli: &mut slack::RtmClient) {
        println!("--- closed");
    }

    fn on_connect(&mut self, cli: &mut slack::RtmClient) {
        println!("--- connected");
    }
}

#[get("/<message>")]
fn root(message: &str) -> String {
    format!("You asked Botket to say: {}", message)
}

fn main() {
    let rocket = rocket::ignite();
    let config = config::active().unwrap();
    let api_key = config.get_str("slack_api_key").unwrap();
    let mut cli = slack::RtmClient::new(api_key);
    let mut botket = Botket;

    let rocket_handle = thread::spawn(|| {
        println!("===== Starting Rocket Thread =====");
        rocket.mount("/", routes![root]).launch();
        "===== Done Rocket Thread ====="
    });

    let slack_handle = thread::spawn(move || {
        println!("===== Starting Slack Client Thread =====");
        let _ = cli.login_and_run::<Botket>(&mut botket);
        "===== Done Slack Client Thread ====="
    });

    println!("{}", rocket_handle.join().unwrap());
    println!("{}", slack_handle.join().unwrap());
}
